package drone.invaders.control.shared;

/**
 * A point in 3d space.
 *
 * @author yiftach
 */
public class Coordinate {

    /* --- Data Members --- */

    private long latitude;

    private long longitude;

    private long altitude;

    /* --- Constructor --- */

    public Coordinate(long latitude, long longitude,  long altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /* --- Access Methods --- */

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }

    public long getAltitude() {
        return altitude;
    }

    @Override
    public String toString() {
        return "Coordinate(" + latitude + ", " + longitude + ", " + altitude + ")";
    }
}
