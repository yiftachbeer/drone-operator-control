package drone.invaders.control.shared.messages;

import drone.invaders.control.shared.Coordinate;

/**
 * Status of the drone.
 *
 * @author yiftach
 */
public class TelemetryMessage {

    /* --- Data Members --- */

    private Coordinate coords;

    private int battery;

    private int satellites;

    /* --- Constructor --- */

    public TelemetryMessage(Coordinate coords, int battery, int satellites) {
        this.coords = coords;
        this.battery = battery;
        this.satellites = satellites;
    }

}
