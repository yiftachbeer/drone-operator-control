package drone.invaders.control.shared.messages;

/**
 * A request to be assigned to a specific ID on the backend (e.g "I want to be drone #3").
 *
 * @author yiftach
 */
// TODO lombok?
public class AssignDroneRequestMessage {

    /* --- Data Members --- */

    /** The requested drone ID. */
    private int droneId;

    /* --- Constructor --- */

    public AssignDroneRequestMessage(int droneId) {
        this.droneId = droneId;
    }

    /* --- Access Methods --- */

    public int getDroneId() {
        return droneId;
    }

    @Override
    public String toString() {
        return "AssignDroneRequestMessage(" + droneId + ")";
    }
}
