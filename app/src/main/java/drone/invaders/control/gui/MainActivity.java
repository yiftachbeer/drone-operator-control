package drone.invaders.control.gui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.gson.GsonBuilder;
import drone.invaders.control.ApplicationLogic;
import drone.invaders.control.R;
import drone.invaders.control.comms.BackendCommunication;
import drone.invaders.control.comms.server.*;
import drone.invaders.control.drone.DroneControl;
import drone.invaders.control.drone.MockupDroneControl;
import drone.invaders.control.shared.Coordinate;
import drone.invaders.control.shared.messages.TelemetryMessage;

import java.io.IOException;
import java.util.LinkedList;

/**
 * First screen, used for selecting a drone number.
 *
 * @author yiftach
 */
public class MainActivity extends AppCompatActivity {

    /* --- Data Members --- */

    private BackendCommunication comms;

    private DroneControl droneControl;

    /* --- AppCompatActivity Overridden Methods --- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeDependencies();

        // TODO Create dynamically from a const
        String[] drones = {"1", "2", "3", "4", "5", "6", "7", "8"};

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,
                drones);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        comms.init();
        droneControl.init();
    }

    /* --- Public Methods --- */

    public void registerId(View view) {
        int droneId = getDroneId();

        try {
            comms.register(droneId);
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void sendCoords(View view) {
        TelemetryMessage message = droneControl.telemetry();
        try {
            comms.sendTelemerty(message);
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    // for testing
    public void updateCoords(View view) {
        long newLat = (long) (Math.random() * 3);
        long newLong = (long) (Math.random() * 3);
        long newAlt = (long) (Math.random() * 3);

        Coordinate coordinate1 = new Coordinate(newLat, newLong, newAlt);

        droneControl.goTo(coordinate1);
    }

    public void connect(View view) {
        try {
            EditText hostBox = (EditText) findViewById(R.id.hostBox);
            String host = hostBox.getText().toString();

            EditText portBox = (EditText) findViewById(R.id.portBox);
            int port = Integer.parseInt(portBox.getText().toString());

            comms.connect(host, port);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Port must be a number", Toast.LENGTH_SHORT).show();
        }
    }

    /* --- Private Methods --- */


    private void initializeDependencies() {
        // Replace with DI
        final SocketManager socketManager = new SocketManager();

        ApplicationLogic logic = new ApplicationLogic(this);

        OutCommunication out = new OutCommunication(new GsonBuilder().create(),
                new LinkedList<>(), logic);

        InCommunication in = new InCommunication(new LinkedList<>(), logic);

        socketManager.setComms(out, in);

        comms = new ServerCommunication(out, in, socketManager);
        logic.setComms(comms);

        droneControl = new MockupDroneControl();
    }

    private int getDroneId() {
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        return spinner.getSelectedItemPosition() + 1;
    }

    public void disconneted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Disconnected!", Toast.LENGTH_LONG).show();
            }
        });
    }
}

