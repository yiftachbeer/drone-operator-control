package drone.invaders.control.comms;

import drone.invaders.control.shared.Coordinate;
import drone.invaders.control.shared.messages.TelemetryMessage;

import java.io.IOException;

/**
 * A mockup {@link BackendCommunication} that does not rely on outside resources.
 *
 * @author yiftach
 */
public class MockupCommunication implements BackendCommunication {

    /* --- BackendCommunication Impl. --- */

    @Override
    public void init() {
        // Do nothing
    }

    @Override
    public void connect(String host, int port) {
        // Do nothing
    }

    @Override
    public void register(int droneId) throws IOException {
        // Do nothing
    }

    @Override
    public void sendTelemerty(TelemetryMessage telemetry) throws IOException {
        // Do nothing
    }

    @Override
    public void reconnect() {
        // Do nothing
    }
}
