package drone.invaders.control.comms.server;

import drone.invaders.control.comms.BackendCommunication;
import drone.invaders.control.shared.Coordinate;
import drone.invaders.control.shared.messages.AssignDroneRequestMessage;
import drone.invaders.control.shared.messages.TelemetryMessage;

import java.io.IOException;

/**
 * Responsible for sending and handling messages with the backend server.
 *
 * @author yiftach
 */
public class ServerCommunication implements BackendCommunication {

    /* --- Data Members --- */

    private OutCommunication out;

    private InCommunication in;

    private SocketManager socketManager;

    /* --- Constructor --- */

    public ServerCommunication(OutCommunication out, InCommunication in, SocketManager socketManager) {
        this.out = out;
        this.in = in;
        this.socketManager = socketManager;
    }

    /* --- BackendCommunication Impl. --- */

    @Override
    public void init() {
        new Thread(out).start();
        new Thread(in).start();
        new Thread(socketManager).start();
    }

    @Override
    public void connect(String host, int port) {
        socketManager.setConnectionDetails(host, port);
    }

    @Override
    public void register(int droneId) throws IOException {
        out.post(new AssignDroneRequestMessage(droneId));
    }

    @Override
    public void sendTelemerty(TelemetryMessage telemetry) throws IOException {
        out.post(telemetry);
    }

    @Override
    public void reconnect() {
        socketManager.requestReconnection();
    }

}
