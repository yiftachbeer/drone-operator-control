package drone.invaders.control.comms.server;

/**
 * Notified on events of disconnection.
 */
public interface DisconnectionListener {

    void disconnected();
}
