package drone.invaders.control.comms.server;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Queue;

/**
 * Continuously iterates over a message queue to send messages through an {@link OutputStream}.
 *
 * @author yiftach
 */
public class OutCommunication implements Runnable {

    /* --- Data Members --- */

    private Gson gson;

    private Queue<Object> messageQueue;

    private DisconnectionListener listener;

    private OutputStream outputStream;

    /* --- Constructor --- */

    public OutCommunication(Gson gson, Queue<Object> messageQueue, DisconnectionListener listener) {
        this.gson = gson;
        this.messageQueue = messageQueue;
        this.listener = listener;
    }

    /* --- Public Methods --- */

    public void post(Object message) {
        Log.d("comms", "Offering: " + message);
        messageQueue.offer(message);
    }

    public void setOutputStream(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /* --- Runnable Impl. --- */

    @Override
    public void run() {
        while (true) {
            if (outputStream != null && !messageQueue.isEmpty()) {
                Object o = messageQueue.peek();
                if (o != null) {
                    String json = toJson(o) + "\r\n";

                    Log.d("comms", "Writing " + json);

                    try {
                        outputStream.write(json.getBytes(Charset.forName("ascii")));

                        messageQueue.remove();
                    } catch (IOException e) {
                        Log.e("comms", "Failed sending " + json + "!", e);
                        outputStream = null;
                        listener.disconnected();
                    }
                }
            }
        }
    }

    /* --- Private Methods --- */

    /**
     * Convert the given object to a JSON, with its class name as the root.
     * @param o The object to convert.
     * @return The created JSON.
     */
    private String toJson(Object o) {
        JsonElement jsonElement = gson.toJsonTree(o);
        JsonObject jo = new JsonObject();
        jo.add(o.getClass().getSimpleName(), jsonElement);
        return jo.toString();
    }

}