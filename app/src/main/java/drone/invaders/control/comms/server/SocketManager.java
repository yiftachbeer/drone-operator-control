package drone.invaders.control.comms.server;

import android.util.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Establishes connections using TCP and notifies an {@link OutCommunication} and an {@link InCommunication}.
 *
 * @author yiftach
 */
public class SocketManager implements Runnable {

    /* --- Constants --- */

    private static final int CONNECTION_TIMEOUT = 5000;

    private static final int SLEEP_PERIOD = 5000;

    /* --- Data Members --- */

    private InCommunication in;

    private OutCommunication out;

    private String host;

    private int port;

    private boolean shouldConnect;

    /* --- Runnable Impl. --- */

    public void run() {
        while (true) {
            int numOfTries = 0;
            while (shouldConnect) {
                if (host != null) {
                    if (numOfTries != 0) {
                        try {
                            Log.i("comms", "Sleeping for " + SLEEP_PERIOD + "...");
                            Thread.sleep(SLEEP_PERIOD);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    Log.i("comms", "Connecting (" + numOfTries + ")...");
                    numOfTries++;

                    connect();
                }
            }
        }
    }

    /* --- Public Methods --- */

    public void setComms(OutCommunication out, InCommunication in) {
        this.out = out;
        this.in = in;
    }

    public void requestReconnection() {
        this.shouldConnect = true;
    }

    public void setConnectionDetails(String host, int port) {
        if (host != this.host || port != this.port) {
            requestReconnection();
        }

        this.host = host;
        this.port = port;
    }

    /* --- Private Methods --- */

    private void connect() {
        try {
            Log.i("comms", "Connecting to " + host + ":" + port + "...");
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(host, port), CONNECTION_TIMEOUT);

            out.setOutputStream(socket.getOutputStream());
            in.setInputStream(socket.getInputStream());

            shouldConnect = false;

            Log.i("comms", "Connected.");
        } catch (IOException e) {
            Log.e("comms", "Could not connect to backend server", e);
        }
    }
}
