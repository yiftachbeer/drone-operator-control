package drone.invaders.control.comms.server;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.nio.charset.Charset;
import java.util.Queue;

/**
 * Reads message from an {@link InputStream} into a message queue, informing a {@link DisconnectionListener}
 * when failing.
 */
public class InCommunication implements Runnable {

    /* --- Data Members --- */

    private InputStream inputStream;

    private Queue<Object> messageQueue;

    private DisconnectionListener listener;

    /* --- Constructor --- */

    public InCommunication(Queue<Object> messageQueue, DisconnectionListener listener) {
        this.messageQueue = messageQueue;
        this.listener = listener;
    }

    /* --- Public Methods --- */

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /* --- Runnable Impl. --- */

    @Override
    public void run() {
        while (true) {
            if (inputStream != null) {
                try {
                    int size;
                    while ((size = inputStream.read()) != -1) {
                        byte[] data = new byte[size];
                        inputStream.read(data);
                        String message = new String(data, Charset.forName("ascii"));

                        //TODO reverse json

                        messageQueue.offer(message);
                    }
                } catch (IOException e) {
                    Log.e("input", "Failed reading", e);
                }
                Log.e("input", "Disconnected.");
                inputStream = null;
                listener.disconnected();
            }
        }
    }

}
