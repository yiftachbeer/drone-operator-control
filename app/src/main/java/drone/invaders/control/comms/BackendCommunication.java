package drone.invaders.control.comms;

import drone.invaders.control.shared.Coordinate;
import drone.invaders.control.shared.messages.TelemetryMessage;

import java.io.IOException;

/**
 * Defines the possible interactions with the backend server.
 *
 * @author yiftach
 */
public interface BackendCommunication {

    void init();

    void connect(String host, int port);

    void register(int droneId) throws IOException;

    void sendTelemerty(TelemetryMessage telemetry) throws IOException;

    void reconnect();

}
