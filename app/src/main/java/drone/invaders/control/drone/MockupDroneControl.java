package drone.invaders.control.drone;

import drone.invaders.control.shared.Coordinate;
import drone.invaders.control.shared.messages.TelemetryMessage;

/**
 * An implementation of {@link DroneControl} that does not really talk to a controller.
 *
 * @author yiftach
 */
public class MockupDroneControl implements DroneControl {

    /* --- Data Members --- */

    private Coordinate currentLocation;

    private int battery;

    private int satellites;
    
    /* --- DroneControl Impl. --- */

    @Override
    public void init() {
        currentLocation = new Coordinate(0, 0, 0);
        battery = 100;
        satellites = 20;
    }

    @Override
    public void goTo(Coordinate... coords) {
        this.currentLocation = coords[coords.length - 1];
    }

    @Override
    public TelemetryMessage telemetry() {
        battery--;
        satellites++;
        return new TelemetryMessage(currentLocation, battery, satellites);
    }
}
