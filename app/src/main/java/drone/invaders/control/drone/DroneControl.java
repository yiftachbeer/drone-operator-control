package drone.invaders.control.drone;

import drone.invaders.control.shared.Coordinate;
import drone.invaders.control.shared.messages.TelemetryMessage;

import java.util.List;

/**
 * Defines possible actions on the drone controller.
 *
 * @author yiftach
 */
public interface DroneControl {

    void init();

    void goTo(Coordinate... coords);

    // should be push, not pull
    TelemetryMessage telemetry();

}
