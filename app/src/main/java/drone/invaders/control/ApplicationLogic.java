package drone.invaders.control;

import drone.invaders.control.comms.BackendCommunication;
import drone.invaders.control.comms.server.DisconnectionListener;
import drone.invaders.control.gui.MainActivity;

/**
 * The "brains".
 *
 * @author yiftach
 */
public class ApplicationLogic implements DisconnectionListener {

    /* --- Data Members --- */

    private BackendCommunication comms;

    private MainActivity gui;

    /* --- Constructor --- */

    public ApplicationLogic(MainActivity gui) {
        this.gui = gui;
    }

    /* --- Access Methods --- */

    public void setComms(BackendCommunication comms) {
        this.comms = comms;
    }

    /* --- DisconnectionListener Impl. --- */

    @Override
    public void disconnected() {
        gui.disconneted();
        comms.reconnect();
    }

}

